
## 项目介绍
该项目为木兰湾管理系统的模板项目，移除了所有的业务功能模块，只包含通用的基础模块开发框架，基于该框架可以快速的进行二次开发。

### 完整功能的木兰湾管理系统项目参见:

[木兰湾项目说明](https://gitee.com/mulanbay)

### 功能简介

* 基于RBAC的用户权限管理
* 支持分布式运行的调度功能
* 统一的日志管理及日志流分析
* 提供磁盘、CPU、内存的监控及报警，并可以自动恢复
* 数据库数据、备份文件自动清理
* 基于微信公众号消息、邮件的消息提醒服务
* 基于错误代码的消息发送可配置化
* 基于Hibernate的配置化的查询便捷封装

### 所用技术

* 前端：Vue、Element UI、Echarts
* 后端：Spring Boot、Hibernate、Quartz、Redis & Jwt

| 核心依赖                | 版本          |
| ---------------------- | ------------- |
| Spring Boot            | 2.3.4.RELEASE |
| Hibernate              | 5.4.21.Final  |
| Quartz                 | 2.3.2         |

### 项目结构
``` lua
mulanbay-server
├── mulanbay-business    -- 通用业务类
├── mulanbay-common      -- 公共模块
├── mulanbay-persistent  -- 持久层基于hibernate的封装
├── mulanbay-pms         -- 木兰湾API接口层
├── mulanbay-schedule    -- 调度模块封装
├── mulanbay-web         -- 基于SpringMVC的一些封装
├── mulanbay-ui          -- 基于Vue的UI

```

### 软件要求
| 软件                    | 版本          |
| ---------------------- | ------------- |
| JDK                    | 1.8+          |
| Nginx                  | 1.17+         |
| Redis                  | 6.0+          |
| Mysql                  | 8.0+          |

### 前端部署文档
#### 开发环境

```
# 克隆项目
git clone https://gitee.com/mulanbay/mulanbay-template.git

# 进入项目目录
cd mulanbay-ui

# 安装依赖
npm install

# 如果安装比较慢，可以选择以下指定源。
# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```
浏览器访问 http://localhost:9528

#### 正式环境

```
# 构建生产环境
npm run build:prod

```
在mulanbay-ui子模块的dist目录下可以看到打包成功的文件

### 后端部署文档

```
# Step 1：初始化数据库

1. 下载源代码
2. 在mysql中创建数据库，比如:mulanbay_db
3. 初始化数据库,执行mulanbay-pms工程docs目录下的sql文件：mulanbay_init.sql

# Step 2：修改配置文件

   在mulanbay-pms/src/main/resources/目录下新建application-local.properties文件，设置本地配置。
   其中Mysql数据库配置、Redis配置为必须配置，如果需要使用微信公众号的消息发送功能，需要配置.

# Step 3：打包&运行

1. 开发环境
  运行mulanbay-pms子工程下的cn.mulanbay.pms.web.Application

2. 正式环境
  * 进入到mulanbay-server目录，运行mvn clean package
  * 运行mulanbay-pms/target下的mulanbay-pms-3.0.jar文件

后端项目默认的端口是：8081
```

本地配置文件application-local.properties范例：

```
# 本地化配置
#系统节点号
system.nodeId=lh
user.avatar.path=D:/test/avatar

#指定为东八区（北京时间）,mysql的高版本驱动会有时区问题
spring.datasource.url=jdbc\:mysql\://127.0.0.1\:3306/mulanbay_db?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8
spring.datasource.username=root
spring.datasource.password=root

# 调度服务
schedule.enable=true
schedule.supportDistri=true

#MD5密码的盐值
security.password.salt=aaabbbccc

#redis.server.password=
redis.server.ip=127.0.0.1
redis.server.password=123456
redis.server.db=5

# 邮件发送配置
mail.username=123456@qq.com
# qq邮箱采用授权码发送，非邮箱密码
mail.password=aaabbb

# 微信相关
wx.appId=wx***
wx.secret=***
wx.token=111222
# 微信消息发送模板编号
wx.userMessageTemplateId=111222

# 微信授权回调地址
wx.accessAuthRedirectUrl=/api/wechat/getWxOpenId
#公众号地址
wx.oaUrl=http://weixin.qq.com/r/111222
```


## 在线演示
暂未提供

## 技术交流
* QQ群：562502224

## 使用&授权

* 源代码100%开源
* 个人使用和公司使用完全免费
  
## 项目截图

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1018/155904_07f4fafc_352331.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/1018/160004_4578ca39_352331.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1018/160129_f6b8c4d6_352331.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/1018/160148_e9286b26_352331.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1018/160213_663ecd15_352331.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/1018/160230_25fafe93_352331.png"/></td>
    </tr>
</table>

