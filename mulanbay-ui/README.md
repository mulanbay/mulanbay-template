## 开发说明

### 所用技术

Vue、Element UI、Echarts

### 开发环境

```bash
# 克隆项目
git clone https://gitee.com/mulanbay-ui-vue

# 进入项目目录
cd mulanbay-ui-vue

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:9527

### 正式环境

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```
